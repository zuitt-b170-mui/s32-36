const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController")
const course = require("../models/course.js")

router.post("/", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the payload (id, email, isAdmin information of the user)
    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})


/* e-commerce websites */

/* 
create a route that will retrieve all of our products/courses
    will require login/register functions
*/

router.get("/", (req,res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// retrieve all active courses
router.get("/active",(req,res) => {
    courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

// retrieve a course
router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId);
    courseController.getCourse(req.params.courseId).then(result => res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req,res) => {
    courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

// archive a course
router.put("/:courseId", auth.verify, (req,res) => {
    courseController.archiveCourse(req.params, req.body).then(result => res.send(result))
})


module.exports = router