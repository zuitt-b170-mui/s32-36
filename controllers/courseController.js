const User = require("../models/user")
const Course = require("../models/course")
const auth = require("../auth")
const bcrypt = require("bcrypt") //used to encrypt user passwords

module.exports.addCourse = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if(userData.isAdmin == false){
            return "You are not an admin"
        }else{
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
            // Saves the created object to the database
            return newCourse.save().then((course, error) => {
                // Course creation failed
                if(error){
                    return false
                }else{
                    // Course creation successful
                    return "Course creation successful"
                }
            })
        }
    })
}


module.exports.getAllCourses = () => {
    return Course.find({}).then((result,error) => {
        if(error){
            return false
        }else{
            return result
        }
    })
}

module.exports.getCourse = (reqParams) =>{
    return Course.findById(reqParams.courseId).then(result =>{
        return result
    })
}

module.exports.getActiveCourses = () => {
    return Course.find( {isActive: true} ).then(result => {
        // will only use res.send if res is a parameter
        return result
    })
}

// update a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result,error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}

// archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        isActive: false
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result,error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}