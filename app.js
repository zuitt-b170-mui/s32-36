const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// server
const app = express();
const port = 4000

// allows all origins/domains to access the backend application
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// defines the routes where the CRUD operations will be executed on the users ("/api/users") and courses ("/api/course")
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

mongoose.connect("mongodb+srv://joshuaniccolo:awesome1@wdc028-course-booking.qwfja.mongodb.net/b170-course-booking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.listen(port, () => console.log(`API now online at port ${port}`))